package springapp;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
/*
    public static void main(String[] args) {
        // create teacher object
        Teacher teacher = new EnglishTeacher();
        // use teacher object
        System.out.println(teacher.getExercise());
    }
*/

    public static void main(String[] args) {
        // laod the spring configuration file
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        // retrieve bean from spring container
        Teacher teacher = context.getBean("mathTeacher", Teacher.class);

        // call methods on the bean
        System.out.println(teacher.getExercise());
        System.out.println(teacher.getTip());

        // close the spring container
        context.close();
    }

}
