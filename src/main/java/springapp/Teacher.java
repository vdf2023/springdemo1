package springapp;

public interface Teacher {
    String getExercise();
    String getTip();
}
