package springapp;

public class FilosophyTeacher implements Teacher, TipsService {

    private TipsService tipsService;

    public FilosophyTeacher(TipsService tipsService) {
        this.tipsService = tipsService;
    }
    public String test(String s1, String s2, String s3){
        return s1+s2+s3;
    }

    @Override
    public String getExercise(){
        return "Who am I?";
    }

    @Override
    public String getTip() {
        return tipsService.getTip();
    }
}
