package springapp;

public class StudyTips implements TipsService{
    @Override
    public String getTip() {
        return "Studying 10 minutes per day is better than 1 hour at the weekend";
    }
}
